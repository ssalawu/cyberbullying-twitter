# README #

This repo contains the cyberbullying and offensive language Twitter dataset created by Semiu Salawu. The dataset was used to train the classifiers used in the BullStop Mobile application (https://play.google.com/store/apps/details?id=free.bullstop.io).

If you use this dataset, please cite as:

Salawu, S., Lumsden, J., & He, Y. (2021). A large-scale English multi-label Twitter dataset for cyberbullying and online abuse detection. In Proceedings of the 5th Workshop on Online Abuse and Harms (WOAH 2021) (pp. 146-156).

You can contact the creator at salawusd[at]aston.ac.uk.